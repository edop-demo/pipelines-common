# pipelines-common

Contain common pipeline code relevant to side cars deployment

# Pipeline Workflow:
```mermaid
graph TD;
    A(Build image in DEV) --> |Push| B(GitLab Container Registry);
    B --> |Pull & Push| C(DEV AWS ECR);
    B --> |Pull & Push| D(QA AWS ECR);
    B --> |Pull & Push| E(CERT AWS ECR);
    B --> |Pull & Push| F(PROD AWS ECR);    
```

Image is pulled from Gitlab container registry to avoid rebuild in higher envs.

# Required variables for this pipeline 
| Key  | Description      |
|------|-------------|
| REGION | AWS Region |
|  APP_IMAGE_VERSION | App executor image used by gitlab runner |
| TEAM_ID | Used when you run the pipeline manually |
| SERVICE\_REQUEST\_NUMBER | sr01576980 |
| STANDARD\_CR\_REPO | Change Request project repo |
| STANDARD\_CR\_PATH | Change Request project path |
| STAGE | Envrionment where the pipeline is run |
| AWS_ACCT_ID_{STAGE} | Account Identifier-ECR Registry |
| ECR_REPO_NAME_{STAGE} | ECR repositry name |
